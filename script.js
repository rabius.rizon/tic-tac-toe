let board = [ '', '', '', '', '', '', '', '', '']
let playerSign = ""
let doesGameStart = false
let turn = false
let result = false

const ws = new WebSocket("ws://localhost:8080/");

ws.onopen = function() {
    // console.log("D")
}

ws.binaryType="arraybuffer"
ws.onmessage = function(event){
    const incomingMessage = JSON.parse(event.data);
    
    // console.log("\nstatus ", playerSign, doesGameStart, turn)
    console.log("ss", incomingMessage)

    if('doesSlotAv' in incomingMessage){
        console.log(event.data)
        return
    }
    
    /// for new connection
    if( 'playerSign' in incomingMessage){
        playerSign = incomingMessage["playerSign"]
        board = incomingMessage['board']
        result = false
        setHeadingSign()
        setheading()
        updateBoardUI()
    }
    
    /// when game start
    if( 'doesGameStart' in incomingMessage){
        doesGameStart = incomingMessage['doesGameStart']
        turn = incomingMessage['turn']
        board = incomingMessage['board']
        updateBoardUI()
        setheading(incomingMessage)
    }
};

function updateBoardUI(){
    // console.log(board)
    for(let i=0; i<tiles.length; i++){
        tiles[i].innerHTML = board[i]
    }
    
}

function userAction(idx){
    if(turn && board[idx]===''){
        board[idx] = playerSign
        updateBoardUI()
        ws.send( JSON.stringify( {'player' : playerSign, 'idx' : idx}))
    }   
}

function setHeadingSign(){
    signElem = document.getElementById("sign")
    signElem.innerHTML = playerSign
}

function setheading(message=null){
    turnElem = document.getElementById("turn")
    if(message && "win" in message){
        clearInterval(downloadTimer)
        result = true
        if(message['win']){
            turnElem.innerHTML = "Congratulations. You win"
        }
        else{
            turnElem.innerHTML = "You lose. Better luck next time."
        }
        return
    }
    if(doesGameStart){
        if(turn){
            showProgressBar()
            turnElem.innerHTML = "Your turn"
        }
        else{
            hideProgressBar()
            turnElem.innerHTML = "Opponent's turn"
        }
    }
    else{
        console.log(doesGameStart)
        // clearInterval(downloadTimer)
        hideProgressBar()
        // clearInterval(downloadTimer)
        turnElem.innerHTML = "Waiting for opponent..."
    }
}

let tiles = Array.from(document.querySelectorAll(".tile"))
tiles.forEach( (element, idx) => {
    element.addEventListener("click", () => {
        userAction(idx)
        clearInterval(downloadTimer)
        hideProgressBar()
    });
});

const resetBtn = document.getElementById("reset")
resetBtn.addEventListener( 'click', ()=>{
    if(result || turn){
        ws.send( JSON.stringify( {'reset' : true}))
        result = false
        clearInterval(downloadTimer)
    }
});

const progressBar = document.getElementById("progressBar")
console.log(progressBar)

function hideProgressBar(){
    progressBar.style.display = "None"
    progressBar.value = "10"
}

function showProgressBar(){
    progressBar.style.display = ""
    var timeLeft = 10;
    downloadTimer = setInterval( () => {
        timeLeft--;
        progressBar.value = timeLeft
        if(timeLeft <= 0){
            turnAfterTimeOut()
        }
    }, 1000);
}

function turnAfterTimeOut(){
    idx = board.indexOf('')
    userAction(idx)
    hideProgressBar()
    clearInterval(downloadTimer)
}