const Websocket = require("ws")
const wss = new Websocket.Server({port:8080})
var CLIENTS = []
var doesMatchStart = false

let board = [ '', '', '', '', '', '', '', '', '']
const winningConditions = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]

const sign = ['X', "O"]
var signId = 0
wss.on("connection", ws => {
    console.log("\nconnected", CLIENTS.length)
    if(CLIENTS.length === 2){
        console.log("Slot full")
        ws.send(JSON.stringify(
            {
                "doesSlotAv" : false
            }))
        return
    }
    
    // for new connected client to server
    ws.send(JSON.stringify(
        {
            "playerSign" : sign[ signId%2 ],
            "board" : board,
        }))
    signId++;
    CLIENTS.push(ws)
    console.log( "size " , CLIENTS.length, doesMatchStart)
    // Match start condition
    if(CLIENTS.length === 2 && !doesMatchStart){
        console.log("Match Start")
        doesMatchStart = true
        startTheGame()
    }
    else if(CLIENTS.length === 1){
        board = [ '', '', '', '', '', '', '', '', '']
        ws.send(JSON.stringify(
        {
            "doesGameStart" : doesMatchStart,
            "board" : board
        }))
    }

    ws.on("message", messages => {
        let message = JSON.parse( `${messages}` )
        console.log(message)
        if('reset' in message){
            startTheGame()
        }

        if('player' in message){
            updateBoard(message)
            const win = checkWinningState(message)
            console.log(win, doesMatchStart)
            if(win === "N"){
                CLIENTS.forEach( client => {
                    if( client !== ws){
                        client.send(JSON.stringify(
                        {
                            "doesGameStart" : doesMatchStart,
                            'turn' : true,
                            "board" : board
                        }))
                    }
                    else{
                        client.send(JSON.stringify(
                        {
                            "doesGameStart" : doesMatchStart,
                            'turn' : false,
                            "board" : board
                        }))
                    }
                });
            }
            else{
                CLIENTS.forEach( client => {
                    if( client === ws){
                        const winPlayer = message["player"] === win? true : false
                        client.send(JSON.stringify(
                        {
                            "doesGameStart" : false,
                            'turn' : false,
                            "board" : board,
                            "win" : winPlayer
                        }))
                    }
                    else{
                        const winPlayer = message["player"] === win? false : true
                        client.send(JSON.stringify(
                        {
                            "doesGameStart" : false,
                            'turn' : false,
                            "board" : board,
                            "win" : winPlayer
                        }))
                    }
                });
            }
            
        }
    });

    ws.on("close", () => {
        CLIENTS.pop(ws)
        console.log("\nclose", CLIENTS.length)
        doesMatchStart = false
        board = [ '', '', '', '', '', '', '', '', '']
        ws.send(JSON.stringify(
        {
            "doesGameStart" : doesMatchStart,
            "board" : board
        }))
    });
});

function updateBoard(data){
    board[ data["idx"] ] = data["player"]
}

function startTheGame(){
    console.log("start the game called")
    board = [ '', '', '', '', '', '', '', '', '']
    firstPlayer = Math.floor((Math.random() * 2))
    secondPlayer = (firstPlayer + 1 ) % 2
    CLIENTS[firstPlayer].send(JSON.stringify(
    {
        "doesGameStart" : doesMatchStart,
        'turn' : true,
        "board" : board
    }))
    CLIENTS[secondPlayer].send(JSON.stringify(
    {
        "doesGameStart" : doesMatchStart,
        'turn' : false,
        "board" : board
    }))
}

function checkWinningState(){
    for(let i=0; i<winningConditions.length; i++){
        condition = winningConditions[i]
        a = board[ condition[0] ]
        b = board[ condition[1] ]
        c = board[ condition[2] ]

        if( a === '' || b === '' || c === ''){
            continue
        }
        else if( a === b && b === c){
            return a      
        }
    }
    return 'N'
}